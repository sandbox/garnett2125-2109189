<?php
/**
 * @file
 * Rules integration for the Commerce Discount module.
 */

/**
 * Implements hook_rules_action_info().
 */
function commerce_discount_gift_rules_action_info() {
  $types = commerce_discount_offer_types();
  $items = array();

  $items[$types['gift_products']['action']] = array(
    'label' => $types['gift_products']['label'],
    'parameter' => array(
      'entity' => array(
        'label' => t('Entity'),
        'type' => 'entity',
        'wrapped' => TRUE,
      ),
      'commerce_discount' => array(
        'label' => t('Commerce discount'),
        'type' => 'token',
        'options list' => 'commerce_discount_entity_list',
      ),
    ),
    'group' => t('Commerce Discount'),
    'base' => $types['gift_products']['action'],
  );

  return $items;
}

/**
 * Rules action: Apply gift products discount.
 *
 * @param EntityDrupalWrapper $wrapper
 *   Wrapped commerce_order entity type.
 * @param string $discount_name
 *   The name of the discount.
 */
function commerce_discount_gift_products(EntityDrupalWrapper $wrapper, $discount_name) {
  $discount_wrapper = entity_metadata_wrapper('commerce_discount', $discount_name);
  if (!$products = $discount_wrapper->commerce_discount_offer->commerce_gift_products->value()) {
    return;
  }

  // Set reference to the discount in order data.
  // Elsewhere it will be deleted
  // by commerce_discount_commerce_cart_order_refresh.
  $order = $wrapper->value();
  $order->data['commerce_discount_gift']['discounts'][$discount_name] = array();
  $discount_product_ids = array();

  $commerce_discount = entity_metadata_wrapper('commerce_discount', $discount_name);
  foreach ($commerce_discount->commerce_discount_offer->commerce_gift_products->value() as $product) {
    // Save the gift in the order so we can build the gift form with these data.
    $order->data['commerce_discount_gift']['discounts'][$discount_name][$product->product_id] = $product;
    // Build an array with all available gift product for this discount.
    $discount_product_ids[] = $product->product_id;
  }

  if (!empty($order->data['commerce_discount_gift']['selected'])) {
    // Loop on products and add each product to order line items.
    foreach ($order->data['commerce_discount_gift']['selected'] as $product_id => $product) {
      // Add condition that the product is in the list of the current discount,
      // otherwise if you have many discount rules, a selected gift will be
      // added to the order (add line item) as much times as existing discount.
      if (in_array($product_id, $discount_product_ids)) {
        $product = commerce_product_load($product_id);
        $discount_name = commerce_discount_gift_get_discount_name($order, $product_id);
        $data = array('context' => array('commerce_discount_offer' => 'gift_products', 'discount_name' => $discount_name));
        $line_item = commerce_product_line_item_new($product, 1, $order->order_id, $data, 'product_discount');
        $wrapper_line_item = entity_metadata_wrapper('commerce_line_item', $line_item);

        // Getting the product price and negate it for the discount component price.
        $product_unit_price = commerce_price_wrapper_value(entity_metadata_wrapper('commerce_product', $product), 'commerce_price');
        $discount_amount = array(
          'amount' => -$product_unit_price['amount'],
          'currency_code' => $product_unit_price['currency_code'],
        );

        commerce_discount_gift_add_price_component($wrapper_line_item, $discount_name, $discount_amount);
        commerce_line_item_save($line_item);

        // Add the free product to order's line items.
        $wrapper->commerce_line_items[] = $line_item;
      }
    }
    // Updating the commerce order calculate price; For some reason, without
    // the following call to commerce_order_calculate_total() the discount
    // components aren't re-added after they get deleted in
    // commerce_discount_commerce_order_presave().
    commerce_order_calculate_total($order);
  }
}

/**
 * Adds a discount price component to the provided line item.
 *
 * @param EntityDrupalWrapper $line_item_wrapper
 *   The wrapped line item entity.
 * @param string $discount_name
 *   The name of the discount being applied.
 * @param array $discount_amount
 *   The discount amount price array (amount, currency_code).
 */
function commerce_discount_gift_add_price_component(EntityDrupalWrapper $line_item_wrapper, $discount_name, $discount_amount) {
  $unit_price = commerce_price_wrapper_value($line_item_wrapper, 'commerce_unit_price', TRUE);
  $discount_wrapper = entity_metadata_wrapper('commerce_discount', $discount_name);
  $component_title = $discount_wrapper->component_title->value();
  $current_amount = $unit_price['amount'];
  // Currencies don't match, abort.
  if ($discount_amount['currency_code'] != $unit_price['currency_code']) {
    return;
  }

  // Calculate the updated amount and create a price array representing the
  // difference between it and the current amount.
  $updated_amount = commerce_round(COMMERCE_ROUND_HALF_UP, $current_amount + $discount_amount['amount']);

  $difference = array(
    'amount' => $discount_amount['amount'],
    'currency_code' => $discount_amount['currency_code'],
    'data' => array(
      'discount_name' => $discount_name,
      'discount_component_title' => empty($component_title) ? 'discount' : $component_title,
    ),
  );

  // Set the amount of the unit price and add the difference as a component.
  $line_item_wrapper->commerce_unit_price->amount = $updated_amount;

  $line_item_wrapper->commerce_unit_price->data = commerce_price_component_add(
    $line_item_wrapper->commerce_unit_price->value(),
    empty($component_title) ? 'discount' : $component_title,
    $difference,
    TRUE
  );
}

/**
 * Get the discount name from a gift selected in the checkout.
 */
function commerce_discount_gift_get_discount_name($order, $product_id) {
  if (!empty($order->data['commerce_discount_gift']['discounts'])) {
    foreach ($order->data['commerce_discount_gift']['discounts'] as $discount_name => $discount_products) {
      if ($discount_name != "selected") {
        foreach ($discount_products as $product) {
          if ($product->product_id == $product_id) {
            return $discount_name;
          }
        }
      }
    }
  }
}