<?php

/**
 * This pane gives all gift available for the current order.
 */
function commerce_discount_gift_pane_checkout_form($form, &$form_state, $checkout_pane, $order) {
  // Get the gift of the commerce discount applied to the order,
  // and create a form with it.
  $checkout_form = array();
  if (!empty($order->data['commerce_discount_gift']['discounts'])) {
    foreach ($order->data['commerce_discount_gift']['discounts'] as $discount) {
      foreach($discount as $product) {
        $product_rendered = entity_view('commerce_product', array($product), 'commerce_discount_gift');
        $checkout_form['commerce_discount_gift_' . $product->product_id] = array(
          '#markup' => drupal_render($product_rendered),
        );
        $checkout_form[$product->product_id] = array(
          '#type' => 'checkbox',
          '#default_value' => FALSE,
        );
      }
    }
    // Reset gift discount so it can be recalculated.
    $order->data['commerce_discount_gift'] = array();
    commerce_order_save($order);
  }
  return $checkout_form;
}

/**
 * Validate that the user didn't choose to many gift.
 */
function commerce_discount_gift_pane_checkout_form_validate($form, $form_state, $checkout_pane, $order) {
  // Get number of product checked as selected gift.
  $products = 0;
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  foreach ($form_state['values']['checkout_gift_on_order'] as $product_id => $checked) {
    if ($checked) {
      $products++;
    }
  }
  // Throw error if the customer chose more gifts than authorised.
  if ($products > commerce_discount_gift_maximum_gift($order_wrapper->commerce_discounts)) {
    form_set_error('commerce_discount_gift', t('You can only choose for %number gift(s) maximum.', array('%number' => commerce_discount_gift_maximum_gift($order_wrapper->commerce_discounts))));
    return FALSE;
  }
  return TRUE;
}

/**
 * Save the selected gift into the order data.
 */
function commerce_discount_gift_pane_checkout_form_submit($form, $form_state, $checkout_pane, $order) {
  // Add the selected gifts to the order data because we can add the line item
  // here or it will be deleted, or the price will be recalculated
  // and no longer be 0.
  $order->data['commerce_discount_gift']['selected'] = array();
  foreach ($form_state['values']['checkout_gift_on_order'] as $product_id => $checked) {
    if ($checked) {
      $order->data['commerce_discount_gift']['selected'][$product_id] = $product_id;
    }
  }
  commerce_order_save($order);
}

/**
 * Get the maximum gift number a customer can choose for an order.
 */
function commerce_discount_gift_maximum_gift($commerce_discounts) {
  $max = 1;
  $commerce_discounts = $commerce_discounts->value();
  if (!empty($commerce_discounts)) {
    foreach ($commerce_discounts as $commerce_discount) {
      $commerce_discount = entity_metadata_wrapper('commerce_discount', $commerce_discount);
      $max = $commerce_discount->commerce_discount_offer->commerce_gift_quantity->value();
    }
  }
  return $max;
}

